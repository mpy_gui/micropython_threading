Welcome to micropython_threadings documentation!
================================================

Generated documentation of mpy_threading powered by doxygen.

mpy_threading is a project to bring the threading library to micropythons PIC32 port.

For running this project following hardware is needed:

* DM320010 - PIC32MZ Embedded Graphics with Stacked DRAM (DA) Starter Kit [info](https://www.microchip.com/DevelopmentTools/ProductDetails/DM320010).
* PIC32MZ2064DAx169 Daughter Card with PIC32MZ2064DAG169 MCU
* MicroSD card
* USB Cable for programming and Power Supply
* USB Cable for serial console

To run on the hardware, load the project into MPLABX, compile the project and program the hardware with an USB cable.

To test threading, use the script threading_test.py which is included into the project folder. Copy the script to the MicroSD card. 
The Script will be executed if you press SW1 on the Starter Kit and the messages will be shown via serial connection (e.g. Putty).


Contact:

software@novotronik.com
