# -*- coding: utf-8 -*-
# Configuration file for the Sphinx documentation builder.
# import sys
# reload(sys)
# sys.setdefaultencoding('utf8')


import os
import sys
sys.path.insert(0, os.path.abspath('.'))
import subprocess
subprocess.call('cd .. ; mkdir build; mkdir build/html; doxygen doxconf', shell=True)
project = 'micropython_threading'
copyright = '2018, Novotronik Software'
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.viewcode',]
source_suffix = '.rst'
master_doc = 'index'
html_theme = 'nature'
html_extra_path = ['../build/html']