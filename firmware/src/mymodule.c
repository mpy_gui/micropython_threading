#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
//#include "portmodules.h"
#include "system_definitions.h"



//STATIC mp_obj_t mymodule_hello(mp_obj_t lednum) {
STATIC mp_obj_t mymodule_hello(mp_obj_t lednum) {
  struct AMessage *pxMessage;
  if( xQueue != 0 )
    {
        /* Send a pointer to a struct AMessage object.  Don't block if the
        queue is already full. */
        pxMessage = & xMessage;
        printf("Toggle: %s \n\r", mp_obj_str_get_str(lednum));
        int len = strlen(mp_obj_str_get_str(lednum));
        printf("len: %i \n\r", len);
        //pxMessage->ucMessageID = mp_obj_get_int(lednum);
        strncpy(pxMessage->ucData, mp_obj_str_get_str(lednum), len);
        pxMessage->ucData[len] = '\0';
        xQueueSend( xQueue, ( void * ) &pxMessage, ( TickType_t ) 0 );
    }
   // printf("\nTicks before: %i", SYS_TMR_TickCountGet());
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(mymodule_hello_obj, mymodule_hello);


STATIC const mp_map_elem_t mymodule_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_mymodule) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_hello), (mp_obj_t)&mymodule_hello_obj },
};

STATIC MP_DEFINE_CONST_DICT (
    mp_module_mymodule_globals,
    mymodule_globals_table
);

const mp_obj_module_t mp_module_mymodule = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_mymodule_globals,
};

