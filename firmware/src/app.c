/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "mpthreadport.h"
#include "py/runtime.h"
#include "sd_card_exec.h"
#include "modio.h"

extern int pyexec_friendly_repl(bool);
// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;

#if 0
    //init framebuffers
    int i;
    for (i = 0; i< NUM_OF_FRAMEBUFFERS; i++){
        framebuffer[i]=(uint8_t*)ddr2_base + FRAMEBUFFER_SIZE;
    }
#endif
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
extern void mp_stack_set_top(void*);
extern void pyexec_continue_repl(void);
extern void gc_init(void *start, void *end);
extern uint8_t ucHeap[ 409600 ];
bool cont_repl;

void APP_Tasks ( void )
{
    
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {     
            cont_repl = false;
            register int sp asm ("sp");
            //printf("%x", sp);
            //uint32_t sp = gc_helper_get_sp();
            // Thread init
            mp_thread_init();
           
            // initialise the stack pointer for the main thread (must be done after mp_thread_init)
            mp_stack_set_top((void*)sp);
            
            gc_init(&ucHeap, &ucHeap[409600]);
            
            mp_init();
            bool appInitialized = true;
       
        
            if (appInitialized)
            {
            
                appData.state = APP_STATE_REPL;
            }
            break;
        }
        case APP_STATE_HANDLE_IRQ:
        {
            //Handle Touch events
            if (isr_enabled.touch){
                callbackFunc_t * current = touch_head;     
                touchData_t tmptouchData;
        NEXT_DATASET:
                isr_enabled.touch = false;
                if (!circBufPop(&circBuf, &tmptouchData)){
                    isr_enabled.touch = true;
                    //element from buffer obtained
                    //now call all listeners with this dataset
                     while (current != NULL){
                        if (current->callbackflag){
                            current->callbackflag = false;
                            //mp_sched_lock();
                            nlr_buf_t nlr;
                            if (nlr_push(&nlr) == 0) {
                                mp_call_function_2(MP_OBJ_FROM_PTR(current->callbackfunc), MP_OBJ_NEW_SMALL_INT(tmptouchData.x), MP_OBJ_NEW_SMALL_INT(tmptouchData.y));
                                nlr_pop()
                            }
                            //mp_sched_unlock();
                        }
                        current = current->next;
                     }
                    goto NEXT_DATASET;
                }else{
                    //buffer is empty
                    isr_enabled.touch  = true;              
                }                               
            }         
            //reset flag
            irq_pending = false;
            cont_repl = true;
            appData.state = APP_STATE_REPL;
            break;
        }      
        case APP_STATE_EXEC_MAIN:
        {
            sd_card_exec_main();
            cont_repl = false;
            appData.state = APP_STATE_REPL;
            break;
        }

        case APP_STATE_REPL:
        { 
            pyexec_friendly_repl(cont_repl);
            break;
        }
        /* TODO: implement your application state machine.*/
        

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

 

/*******************************************************************************
 End of File
 */
