/*
 File Created by Module and Class Generator for Micropython
 Author: Adam Lang 
 More information about Micropython modules: 
 https://forum.micropython.org/viewtopic.php?t=3274
 http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html
*/

#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "system_definitions.h"

// MODULE GLOBAL TABLE (all classes, all static methods)
STATIC const mp_map_elem_t sys_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_sys) },
    { MP_ROM_QSTR(MP_QSTR_path), MP_ROM_PTR(&MP_STATE_VM(mp_sys_path_obj)) },
    { MP_ROM_QSTR(MP_QSTR_argv), MP_ROM_PTR(&MP_STATE_VM(mp_sys_argv_obj)) },
};
STATIC MP_DEFINE_CONST_DICT(mp_module_sys_globals, sys_globals_table);


// ADD MODULE TO MODULE GLOBALS (do not edit)
const mp_obj_module_t mp_module_sys = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_sys_globals,
};