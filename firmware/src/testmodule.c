#include <stdint.h>
#include <unistd.h>

#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "bsp.h"
#include "gfx/hal/inc/gfx_common.h"
#include "gfx/hal/inc/gfx_draw_rect.h"
#include "gfx/hal/inc/gfx_draw.h"
#include "gfx/hal/gfx.h"
#include "stdlib.h"
#include "gfx/hal/inc/gfx_math.h"
#include "system_definitions.h"

#include "gfx/hal/inc/gfx_default_impl.h"
#include "gfx/driver/controller/glcd/drv_gfx_glcd_static.h"
extern int pyexec_file(const char*); 
#if 0
    struct BMessage *pxRxBMessage;
    void myVSYNCCallback (void){

        if( xQueueReceive( xQueueFramebuf, &( pxRxBMessage ), ( TickType_t ) 0 ) )
        {      
            Nop();
            PLIB_GLCD_LayerBaseAddressSet(GLCD_ID_0, 0, pxRxBMessage->cur_framebuffer);
        }    
    }
#endif

#define FB_ALIGNMENT  4608000
void* framebuf_alloc(size_t bytes){
    return dlmemalign(FB_ALIGNMENT, bytes);
}

void * framebuf_calloc(size_t n_elements, size_t elem_size){
    void* mem;
    size_t req = 0;
    if (n_elements != 0) {
        req = n_elements * elem_size;
        if (((n_elements | elem_size) & ~(size_t)0xffff) &&
            (req / n_elements != elem_size))
                req = (~(size_t)0); /* force downstream failure on overflow */
    }
  mem = framebuf_alloc(req);
  if (mem != 0 )
    memset(mem, 0, req);
  return mem;
}



bool mptask_init_sd_filesystem (void){
    if(SYS_FS_Mount("/dev/mmcblka1", "/sd", FAT, 0, NULL) == 0)
    {
        //mounting successful
        return true;
    }
    else{
        printf("\n\rMounting error!");
        return false;
    }   
}


void testInit(void){
    static bool fs_mounted = false;
    if (!fs_mounted){
            fs_mounted = mptask_init_sd_filesystem();
            mp_obj_list_init(mp_sys_path, 0);
            mp_obj_list_init(mp_sys_argv, 0);
            mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR_)); // current dir (or base dir of the script)
            mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR__slash_sd));
            mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR__slash_sd_slash_lib));
    }
    // reset config variables; they should be set by boot.py
    const char *main_py;
    main_py = "main.py";
    pyexec_file(main_py);
}   



void draw_overlay(uint32_t * ptr){
    int w,h;
    //draw overlay
    for (w = 0; w< 480; w++){
        for (h = 0; h< 272; h++){
            if (h == 0 || h == 136 || h == 271 ){
                ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_LIGHTGRAY); 
            }else if (w == 0 || w == 479 || w == 239){
                ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_LIGHTGRAY);
            }  
        }  
    }  
    
}

void fill_field(uint32_t * ptr, uint8_t fieldnum){
    int w,h;
    for (w = 0; w< 480; w++){
        for (h = 0; h< 272; h++){
                ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_BLUE); 
        }  
    }   
    static GFX_Color color = GFX_COLOR_BLACK;
    
    switch (fieldnum){
        case 1:
            for (w = 0; w< 240; w++){
                for (h = 0; h< 136; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        case 2:
            for (w = 240; w< 480; w++){
                for (h = 0; h< 136; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        case 3:
            for (w = 240; w< 480; w++){
                for (h = 136; h< 272; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        case 4:
            for (w = 0; w< 240; w++){
                for (h =  136; h< 272; h++){
                    ptr[h*480 + w] = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , color); 
                }  
            } 
            break;
        default:
            printf("D");
            break;                 
    }
    color = (color + 1) % GFX_COLOR_LAST;
    draw_overlay(ptr);
}

STATIC mp_obj_t testmodule_blink(void) {
    //BSP_LEDToggle(BSP_LED_1);
    int x, x2;
    int y, y2;
    int w;
    int h;
    int col;
    int i;
    int buf_nr;
    //struct BMessage *pxMessage;
    i = 0;
    uint32_t val, val2, res;  
    GFX_PixelBuffer* mybuf;
    GFX_PixelBuffer* mybuf2;
    GFX_Rect* myrect;
    GFX_Rect* myrect2;
    static int call = 0;
    call = (call+1) % 3;
    uint32_t * ptr1;
    uint32_t * ptr2;
    uint32_t * ptr3;
    uint32_t * adress = (uint32_t *) 0xA8000000;
    //GFX_Color GFX_ColorValue(GFX_ColorMode mode, GFX_ColorName name)

    ptr1 = dlmemalign(FB_ALIGNMENT, 480*272* sizeof (GFX_Color));
    printf("\n\r ptr1: %x", ptr1);
    ptr2 = dlmemalign(FB_ALIGNMENT, 480*272* sizeof (GFX_Color));
    printf("\n\r ptr2: %x", ptr2);
    ptr3 = dlmemalign(FB_ALIGNMENT, 480*272* sizeof (GFX_Color));
    printf("\n\r ptr3: %x", ptr3);
    uint32_t * framebuffer = (uint32_t *)GLCDL0BADDR;
    printf("\n\rframebuffer: %x\r\n", framebuffer);
/*
    
    ptr1 = dlmalloc(480*272* sizeof (GFX_Color));
    printf("\n\r ptr1: %x", ptr1);
    ptr2 = dlmalloc(480*272* sizeof (GFX_Color));
    printf("\n\r ptr2: %x", ptr2);
    ptr3 = dlmalloc(480*272* sizeof (GFX_Color));
    printf("\n\r ptr3: %x", ptr3);
    uint32_t * framebuffer = (uint32_t *)GLCDL0BADDR;
    printf("\n\rframebuffer: %x\r\n", framebuffer);
  */  
    //memset(ptr1, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_LIME), 480*272* sizeof(GFX_Color));
   // memset(ptr2, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_LIME),480*272* sizeof(GFX_Color));
    //memset(ptr3, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888 , GFX_COLOR_CYAN),480*272* sizeof(GFX_Color)); 

    testInit();

    
    
    
    
    
#define state_INIT 1
#define state_A 2
#define state_B 3
#define state_C 4    
#define state_D 5
        
    uint8_t state = state_INIT;

    for (i = 0; i< 10; i++){
        switch (state){
            case state_INIT:
                fill_field(ptr1, 1);
                state = state_A;
                break;
            case state_A:
                GLCDL0BADDR = KVA_TO_PA(ptr1);
                fill_field(ptr2, 2);
                state = state_B;
                break;
            case state_B:
                GLCDL0BADDR = KVA_TO_PA(ptr2);
                fill_field(ptr1, 3);
                state = state_C;
                break;            
            case state_C:
                GLCDL0BADDR = KVA_TO_PA(ptr1);
                fill_field(ptr2, 4);
                state = state_D;                         
                break;            
            case state_D:
                GLCDL0BADDR = KVA_TO_PA(ptr2);
                fill_field(ptr1, 1);
                state = state_A;                
                break;
            default:
                printf("D");
                break;
        } 
    }
#if 0   
    //SD Card test
    
    char filename [60];
    int result = -1;
    sprintf(filename, "/mnt/myDrive/test.txt");
    SYS_FS_HANDLE outfile;
    if(SYS_FS_Mount("/dev/mmcblka1", "/mnt/myDrive", FAT, 0, NULL) == 0)
    {
        //mounting successful
    }
    else{
        printf("\n\rMounting error!");
        goto END;
    }
    if ((outfile=SYS_FS_FileOpen("tesst.txt", SYS_FS_FILE_OPEN_WRITE)) != SYS_FS_HANDLE_INVALID)
    {
        //opening file successful
    }else{
        printf("\n\rFile opening error!");
        goto END;
    }
    SYS_FS_FilePrintf(outfile, "DAS IST EIN TEST");
    
    
    //close file and unmount drive
    do{
        result = SYS_FS_FileClose(outfile);
    }while( result );

    if (SYS_FS_Unmount("/mnt/myDrive") != SYS_FS_RES_SUCCESS)
    {
    }
    printf("\n\r file closed with success!");

    END:
                printf("\n\rLabel END reached");
    
#endif
 
    
#if 0  
    //#define EXEC_FLAG_SOURCE_IS_FILENAME (32)
//test python script from sd card
    if(SYS_FS_Mount("/dev/mmcblka1", "/mnt/myDrive", FAT, 0, NULL) == 0)
    {
        //mounting successful
    }
    else{
        printf("\n\rMounting error!");
        goto END;
    }
    
    res = pyexec_file("test.py");
    printf("\r\n result: %i",res);
    /*
    int pyexec_file(const char *filename) {
    return parse_compile_execute(filename, MP_PARSE_FILE_INPUT, EXEC_FLAG_SOURCE_IS_FILENAME);
}
    
    */
    END:
        printf("\n\rLabel END reached");  
#endif    
    
#if 0
    for (i = 0; i< 100; i++) {
        printf(".%i.", i);
       switch (i%4){
        case 0:
            GLCDL0BADDR = KVA_TO_PA(ptr1);
            printf("0");
            break;
        case 1:
            GLCDL0BADDR = KVA_TO_PA(ptr2);
            printf("1");
            break;
        case 2:
            GLCDL0BADDR = KVA_TO_PA(ptr3);
            printf("2");
            break;
        default:
            printf("D");
            break;    
        }
       int u;
       for (u = 0; u <10000; u++)
           Nop();
    }
#endif
    
    
    dlfree(ptr1);
    dlfree(ptr2);
    dlfree(ptr3);
    
    /*
    for (i = 0; i< 480*272; i++){
        if (i < 20)
            printf("\r\n1: %x, %x | 2: %x, %x| 3: %x, %x", ptr1, *ptr1, ptr2, *ptr2, ptr3, *ptr3);
        ptr1++;
        ptr2++;
        ptr3++;

    }
    */
   
    printf("\r\nGLCDL0STRIDE %x",GLCDL0STRIDE);
    printf("\r\nGLCDL0RES %x",GLCDL0RES);
    printf("\r\nGLCDL0SIZE %x",GLCDL0SIZE);
    printf("\r\nGLCDL0START %x",GLCDL0START);
    printf ("\n\rGLCDL0MODE %x", GLCDL0MODE);
    printf("\n\rframebuffer GLCDL0BADDR: %x\n\r", GLCDL0BADDR);

    //Set background color Blue
    //GLCDBGCOLOR = 0xF000;
#if 0
    GFX_Begin();
    GFX_End();
    GFX_Update();
#endif    
    

    
    /*
    memcpy(framebuffer, ptr2, 100*100* sizeof (GFX_Color));
    for (i = 0; i< 100000; i++){
        Nop();
    }
    printf("2");
    memcpy(framebuffer, ptr3, 100*100* sizeof (GFX_Color));
    for (i = 0; i< 100000; i++){
        Nop();
    }
    printf("3");
 
    for (x = 0; x< 100; x++){
        printf("\t %x: %x", adress, *adress);
        adress++;
    }
    */
    
    
    
    
    
    
    
    
    
    
    
    
    
#if 0
    static bool activated = false;
    uint8_t * var = (uint8_t*) 0x88000000;
    
    uint8_t * buffer;
    for (i = 0; i < 5; i++){
        buffer = dlmalloc(100*sizeof(uint8_t));
        printf("\n\rallocation ok, pointer address: %x", buffer);
        *buffer = i;
    }
    printf("\n\r");
    for (i = 0; i< 5; i++ ){
        for (x = 0; x< 16; x++){
            printf("%x :%x   ", var, *var);
            var++;
        }
        printf("\n\r");
    }
    dlfree(buffer);
    printf("\n\r");
    var = (uint8_t*) 0x88000000;
    for (i = 0; i< 5; i++ ){
        for (x = 0; x< 16; x++){
            printf("%x :%x   ", var, *var);
            var++;
        }
        printf("\n\r");
    }
#endif
   // buffer = dlmalloc(10*sizeof(uint8_t));
   // printf("\n\rallocation ok, pointer address: %x", buffer);
    //dlfree(buffer);
   // buffer = dlmalloc(32*100*sizeof(uint8_t));
   // printf("\n\rallocation ok, pointer address: %x", buffer);  
#if 0 
    GFX_Set(GFXF_LAYER_ACTIVE, 0);
    
    while(1){
        uint32_t pixCnt;  
        uint32_t * currPix;
        uint32_t activeColor = rand() % 17;
        activeColor = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col); 
        
        for(pixCnt = 0 ;pixCnt < FRAMEBUFFER_SIZE  / 4; pixCnt++){
            currPix = framebuffer[i] + pixCnt;
            * currPix = activeColor;
        }
           
        if( xQueueFramebuf  != 0 )
          {
              /* Send a pointer to a struct AMessage object.  Don't block if the
              queue is already full. */
              pxMessage = & BMessage_t;
              pxMessage->cur_framebuffer = framebuffer[i];
              xQueueSend( xQueueFramebuf, ( void * ) &pxMessage, ( TickType_t ) 0 );
          }
        
        i = (i++)% 64;
        
        
        if (!activated)
            defVSyncCallbackSet(&myVSYNCCallback);

        
       vTaskDelay(100 / portTICK_PERIOD_MS); 
    }
#endif    
    
#if 0
    uint32_t val, val2, res;  
    GFX_PixelBuffer* mybuf;
    GFX_PixelBuffer* mybuf2;
    GFX_Rect* myrect;
    GFX_Rect* myrect2;
    Nop();
    printf("\nmypointer points to: %x", ddr2_base);
    printf("\nfilling buffer of 32MB...");

    for (i = 0; i < 32*1024*1024; i++){
        *ddr2_base = 0xA;
        ddr2_base++;
    }
    ddr2_base = (uint8_t*) 0x88000000;
    printf("\nfilling buffer complete, now testing...");
    for (i = 0; i < 32*1024*1024; i++){
        if(*ddr2_base != 0xA){
            printf("\nError!");
            break;
        }
        ddr2_base++;
    }
    ddr2_base = (uint8_t*) 0x88000000;
    printf("\ntest complete\n");
#endif
#if 0  
    *mypointer = 0x0000;
    printf("\nvalue there: %x", *mypointer);
    *mypointer = 0xAFFE;
    printf("\nvalue there: %x", *mypointer);
#endif  
   
#if 0
#define SIZE 50
    mybuf = dlmalloc(sizeof(GFX_PixelBuffer));
    if (mybuf == NULL)
        printf("\nerror in malloc1!");
        
    mybuf2 = dlmalloc(sizeof(GFX_PixelBuffer));
    if (mybuf2 == NULL)
        printf("\nerror in malloc1!");
    
    myrect = dlmalloc(sizeof(GFX_Rect));
    if (myrect == NULL)
        printf("\nerror in malloc2!");
    
    myrect2 = dlmalloc(sizeof(GFX_Rect));
    if (myrect2 == NULL)
        printf("\nerror in malloc2!");
    
    myrect->height = SIZE;
    myrect->width = SIZE;
    myrect->x = 0;
    myrect->y = 0;
    
    myrect2->height = SIZE;
    myrect2->width = SIZE;
    myrect2->x = 0;
    myrect2->y = 0;

    GFX_Color * buf_add;
    GFX_Color * buf_add2;
    
    buf_add = dlmalloc (SIZE*SIZE* sizeof(GFX_Color));
    if (buf_add == NULL)
        printf("\nerror in malloc GFX Color!");
    buf_add2 = dlmalloc (SIZE*SIZE* sizeof(GFX_Color));
    if (buf_add2 == NULL)
        printf("\nerror in malloc GFX Color!");
    
    res = GFX_PixelBufferCreate(SIZE, SIZE, GFX_COLOR_MODE_RGBA_8888 , buf_add, mybuf);
    if (res != GFX_SUCCESS )
        printf("\nerror in GFX_PixelBufferCreate!");
    
    res = GFX_PixelBufferCreate(SIZE, SIZE, GFX_COLOR_MODE_RGBA_8888 , buf_add2, mybuf2);
    if (res != GFX_SUCCESS )
        printf("\nerror in GFX_PixelBufferCreate!");
    res = GFX_PixelBufferAreaFill(mybuf,  myrect ,GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_LIME));
    if (res != GFX_SUCCESS )
         printf("\nerror in GFX_PixelBufferAreaFill!");
    
    res = GFX_PixelBufferAreaFill(mybuf2,  myrect2 ,GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_WHITE));
    if (res != GFX_SUCCESS )
         printf("\nerror in GFX_PixelBufferAreaFill!");
    
    GFX_Set(GFXF_LAYER_ACTIVE, 0);   
    GFX_Begin();
    
    for ( x = 0; x < 10; x++){
        printf("x: %i", x);
        for( y=0; y < 272-SIZE; y++){
            res = GFX_DrawBlit(mybuf,0,0,SIZE,SIZE,x,y);
            res = GFX_DrawBlit(mybuf2,0,0,SIZE,SIZE,x,y);
        }
    }
    
    if (res != GFX_SUCCESS )
         printf("\nerror in GFX_DrawBlit!");
     GFX_End();

     
     
     
    uint32_t tmr;
    tmr = SYS_TMR_TickCountGet();
    v
    for (i = 0; i< 30; i++){
        GFX_Begin();
        x = rand() % 480;
        x2= x;
        while (x2 == x)
           x2 = rand() % 480; 
        
        y = rand() % 272;
        y2 = y;
        while (y2 == y)
            y2 = rand() % 272;
        col = rand() % 17;
        w = rand() % 200;
        h = rand() % 100;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));
        GFX_DrawLine( x, y, x2, y2);
        GFX_DrawRect(x,y, w,h);
        w = rand() % 200;
        //circle could not be drawn out of display bounds
        if ( x+w > 479 || x-w < 1 || y+w > 271 || y-w < 1)
            w = GFX_Mini( GFX_Mini(479-x, x-1), GFX_Mini(271-y,y-1));
        
        col = rand() % 17;
        GFX_Set(GFXF_DRAW_COLOR, GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col));            
        GFX_DrawCircle(x, y, w);  
        GFX_End();
    }
    
    Nop();
    printf("Ticks: %i",SYS_TMR_TickCountGet() - tmr );
    
     
     
     
     
     
     
#endif
     
     
     
     
     
     
     
     
#if 0 
  
    //fill each buffer with other 
    for (buf_nr = 0; buf_nr < 1; buf_nr++){
        
        col = rand() % 17;
        col = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, col); 
        
        for(x=0; x < 480; x++){
            for (y= 0; y<272; y++){
                framebuffers[x][y] = col;
            }
        }
    }
    printf("\nBuffer fill complete!");
    
    GFX_Begin();
    for (buf_nr = 0; buf_nr < 1; buf_nr++){
        GFX_DrawBlit(&framebuffers[0][0],0,0,480,272,0,0);
    }
    
    GFX_End();
    
#endif
    



    /*
    GFX_DrawState drawstate;
    drawstate.color = GFX_ColorValue(GFX_COLOR_MODE_RGBA_8888, GFX_COLOR_WHITE);
    drawstate.colorMode = GFX_COLOR_MODE_RGBA_8888;
    drawstate.thickness = 1;
    GFX_Rect fill;
    fill.x = 0;
    fill.y = 0;
    fill.height = 272;
    fill.width = 480;
    cpuDrawRect_Fill(&fill, &drawstate); 
    */
    
    //random lines
   // GFX_Set(GFXF_LAYER_ACTIVE, 0);
   // GFX_Set(GFXF_DRAW_MODE, GFX_DRAW_LINE); 


    //
    //random rectangles
  //  GFX_Set(GFXF_LAYER_ACTIVE, 0);
  //  GFX_Set(GFXF_DRAW_MODE, GFX_DRAW_LINE); 
//    uint32_t tmr;
  //  tmr = SYS_TMR_TickCountGet();
//    GFX_Set(GFXF_LAYER_ACTIVE, 1);

   //  printf("Ticks: %i",SYS_TMR_TickCountGet() - tmr );
    
   // GFX_Set(GFXF_DRAW_PIPELINE_MODE, GFX_PIPELINE_SOFTWARE);

    //random circles
    //GFX_Set(GFXF_LAYER_ACTIVE, 2);
   // GFX_Set(GFXF_DRAW_PIPELINE_MODE, GFX_PIPELINE_GCUGPU);
 //   GFX_Set(GFXF_LAYER_ACTIVE, 0);
 //   GFX_Set(GFXF_DRAW_MODE, GFX_DRAW_LINE);
//    uint32_t tmr;
  //  tmr = SYS_TMR_TickCountGet();

   // printf("Ticks: %i",SYS_TMR_TickCountGet() - tmr );
    
 //   GFX_Get(GFXF_BRIGHTNESS_RANGE, &val, &val2);
 //  printf("\nval: %i, val: %i", val, val2);
 //   res = GFX_Set(GFXF_LAYER_ENABLED, 1);
  //  GFX_Set(GFXF_LAYER_ACTIVE, 0);
  //  res = GFX_Get(GFXF_LAYER_ACTIVE, &val);
    
    //GFX_Set(GFXF_LAYER_ENABLED, 0);

    //res = GFX_Set(GFXF_LAYER_ENABLED, 1);
 //   printf("\nres: %i", res);
  //  printf("\nval: %i", val);
    
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(testmodule_blink_obj, testmodule_blink);

STATIC const mp_map_elem_t testmodule_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_testmodule) },
    { MP_OBJ_NEW_QSTR(MP_QSTR_blink), (mp_obj_t)&testmodule_blink_obj },
};

STATIC MP_DEFINE_CONST_DICT (
    mp_module_testmodule_globals,
    testmodule_globals_table
);

const mp_obj_module_t mp_module_testmodule = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_testmodule_globals,
};