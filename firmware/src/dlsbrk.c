/* 
 * dlsbrk.c: sbrk emulation for use with dlmalloc.
 */
#include <string.h>         //include string.h for memset
#include <sys/types.h> /* ssize_t */


static uint8_t * curbrk = 0;
static uint8_t * minva = (uint8_t *) 0x88000000; 		//points to the start of ddr2 sdram in kseg0 (virtual address);
static uint8_t * maxva = (uint8_t *) 0x89FFFFFF; 		//points to the end of ddr2 sdram in kseg0 (virtual address);
//static uint8_t * minva = (uint8_t *) 0xA8000000 ; 		//points to the start of ddr2 sdram in kseg1 (virtual address);
//static uint8_t * maxva = (uint8_t *) 0xA9FFFFFF ; 		//points to the end of ddr2 sdram in kseg1 (virtual address);

void *
_dlsbrk (int n)
{
    uint8_t * newbrk;
	uint8_t * p;


    if (!curbrk)
		curbrk  = minva;

    p = curbrk;
    newbrk = curbrk + n;
    if (n > 0) {
	if (newbrk > maxva) {
	    return (void *)-1;
	}
    }
    curbrk = newbrk;
    return p;
}

void *
dlsbrk (int n)
{
    void *p;

    p = _dlsbrk(n);

    /* sbrk defined to return zeroed pages */
    if ((n > 0) && (p != (void *)-1))
	memset (p, 0, n);

    return p;
}
